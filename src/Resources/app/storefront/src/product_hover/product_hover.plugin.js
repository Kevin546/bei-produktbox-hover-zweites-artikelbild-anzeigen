import Plugin from 'src/plugin-system/plugin.class';

export default class ProductHover extends Plugin {
    init() {
        
        this.cardBody = document.querySelectorAll('.card-body');

        this.firstProductImage = document.querySelectorAll('.first-product-image');      
        this.secondProductImage = document.querySelectorAll('.second-product-image');
     

         for(let i = 0; i < this.cardBody.length; i++)
         {  
            if (this.cardBody[i].dataset.secondImage == "true"){
                this.cardBody[i].addEventListener('mouseover', (e) => {
                    this.showSecondProductImage(i);
                    });
                    this.cardBody[i].addEventListener('mouseout', (e) => {
                        this.hideSecondProductImage(i);
                    });
            }
         }
    }

    showSecondProductImage(i){

        this.firstProductImage[i].style.display = "none";
        this.secondProductImage[i].style.display = "block";

        //this.secondProductImage[i].innerHTML = '{% sw_thumbnails "product-second-image-thumbnails" with { media: product_second_image.media, sizes: {"xs": "501px", "sm": "315px", "md": "427px", "lg": "333px", "xl": "284px"}} |e("js")|raw %}';

        let secondImageurl = this.secondProductImage[i].dataset.secondImageurl;
        this.secondProductImage[i].innerHTML = '<img class="secondImageContent" src="'+secondImageurl+'">';
    }
    hideSecondProductImage(i){

        this.firstProductImage[i].style.display = "block";
        this.secondProductImage[i].style.display = "none";
    }
}
