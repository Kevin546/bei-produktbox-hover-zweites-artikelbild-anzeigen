import ProductHover from './product_hover/product_hover.plugin';

const PluginManager = window.PluginManager;
PluginManager.register('ProductHover', ProductHover, '[product-hover-plugin]');