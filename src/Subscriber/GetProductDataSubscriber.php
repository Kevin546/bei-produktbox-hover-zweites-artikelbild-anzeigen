<?php declare(strict_types=1);

namespace ShowProductImageOnHover\Subscriber;

use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;

class GetProductDataSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            ProductListingCriteriaEvent::class =>  "onProductsLoaded"
        ];
    }

    public function onProductsLoaded(ProductListingCriteriaEvent $event)
    {
        
        $event->getCriteria()->addAssociation('media');
       
    }
}